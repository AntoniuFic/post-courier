<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function continent(){
        return $this->belongsTo('App\Continent');
    }
    public function users(){
        return $this->hasMany('App\User');
    }
    public function zipcodes(){
        return $this->hasMany('App\ZipCode');
    }
    public function offices(){
        return $this->hasMany('App\Office');
    }
    public function items(){
        return $this->hasMany('App\Item');
    }
    public function hubs(){
        return $this->hasMany('App\Hub');
    }
}
