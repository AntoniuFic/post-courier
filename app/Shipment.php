<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipment extends Model
{
    public function awbs(){
        return $this->hasMany('App\AWB');
    }
    public function transport(){
        return $this->belongsTo('App\Transport');
    }
}
