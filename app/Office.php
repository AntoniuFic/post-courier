<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    public function zipcodes(){
        return $this->hasMany('App\ZipCode');
    }
    public function awbs(){
        return $this->hasMany('App\AWB');
    }
    public function items(){
        return $this->hasMany('App\Item');
    }
    public function country(){
        return $this->belongsTo('App\Country');
    }
    public function users(){
        return $this->hasMany('App\User');
    }
    public function hub(){
        return $this->belongsTo('App\Hub');
    }
}
