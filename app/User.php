<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'surname', 'street', 'number', 'block', 'staircase', 'apartment', 'city', 'county', 'country_id', 'zipcode_id', 'office_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function zipcode(){
        return $this->belongsTo('App\ZipCode');
    }
    public function watches(){
        return $this->hasMany('App\Watch');
    }
    public function country(){
        return $this->belongsTo('App\Country');
    }
    public function office(){
        return $this->belongsTo('App\Office');
    }
    public function feedbacks(){
        return $this->hasMany('App\Feedback');
    }
}
