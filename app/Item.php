<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'name', 'surname', 'street', 'number', 'block', 'staircase', 'apartment', 'city', 'county', 'country_id', 'zipcode_id', 'office_id', 'awb_id', 'weight', 'volume', 'price', 'priority'
    ];
    public function office(){
        return $this->belongsTo('App\Office');
    }
    public function zipcode(){
        return $this->belongsTo('App\ZipCode');
    }
    public function awb(){
        return $this->belongsTo('App\AWB');
    }
    public function watches(){
        return $this->hasMany('App\Watches');
    }
    public function country(){
        return $this->belongsTo('App\Country');
    }
}
