<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $primaryKey = 'id', $table='feedbacks';
    public function user(){
        return $this->belongsTo('App\User');
    }
}
