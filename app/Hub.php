<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hub extends Model
{
    public function country(){
        return $this->belongsTo('App\Country');
    }
    public function offices(){
        return $this->hasMany('App\Office');
    }
}
