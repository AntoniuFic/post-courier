<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function contact(Request $request){
        $data = $request->validate([
            'subject' => 'required|string|max:100|min:5',
            'contents' => 'required|string|min:5'
        ]);
        $feedback = new Feedback;
        $feedback->content = $data['contents'];
        $feedback->subject = $data['subject'];
        $feedback->user_id = Auth::id();
        $feedback->save();
        return redirect('/');
    }

}
