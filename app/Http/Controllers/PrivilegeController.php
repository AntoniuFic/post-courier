<?php

namespace App\Http\Controllers;

use App\AWB;
use App\Continent;
use App\Country;
use App\Fare;
use App\Feedback;
use App\Hub;
use App\Item;
use App\News;
use App\Office;
use App\User;
use App\ZipCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Validation\Rule;

class PrivilegeController extends Controller
{
    public function registerEmployeeForm(){
        $offices = null;
        if(Auth::user()->permission_level == 3)
            $offices = Office::where('id', Auth::user()->office->id)->get();
        else if(Auth::user()->permission_level == 4)
            $offices = Office::where('hub_id', Auth::user()->office->hub->id)->get();
        else if(Auth::user()->permission_level == 5 || Auth::user()->permission_level == 6)
            $offices = Office::where('country_id', Auth::user()->office->country->id)->get();
        else $offices = Office::all();
        return view('auth.register-employee')->with('options', $offices);
    }
    protected function registerEmployee(Request $request){
        $data = $request->validate([
            'name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'surname' => ['required', 'string', 'max:100'],
            'street' => ['required', 'string', 'max:100'],
            'number' => ['required', 'string', 'max:100'],
            'block' => ['nullable', 'string', 'max:100'],
            'staircase' => ['nullable', 'string', 'max:100'],
            'apartment' => ['nullable', 'string', 'max:100'],
            'city' => ['required', 'string', 'max:100'],
            'county' => ['required', 'string', 'max:100'],
            'country' => ['required', 'integer'],
            'zipcode' => ['required', 'string', 'max:100'],
            'permissionlevel' => ['required', 'integer', 'max:7'],
            'office' => ['required', 'integer'],
        ]);
        $zip = ZipCode::where('country_id', $data['country'])->where('zip', $data['zipcode'])->first();
        $user = new User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->surname = $data['surname'];
        $user->street = $data['street'];
        $user->number = $data['number'];
        $user->block = $data['block'];
        $user->staircase = $data['staircase'];
        $user->apartment = $data['apartment'];
        $user->city = $data['city'];
        $user->county = $data['county'];
        $user->country_id = $data['country'];
        $user->zipcode_id = $zip->id;
        $user->office_id = $data['office'];
        $user->permission_level = intval($data['permissionlevel']);
        $user->save();
        $user->sendEmailVerificationNotification();
        return redirect('/manager');
    }

    protected function manager(){
        $officeEmployees = User::where('office_id', Auth::user()->office->id)->where('permission_level', '>', 1)->where('permission_level', '<', Auth::user()->permission_level)->get();
        return view('privilege.manager')->with([
            'officeEmployees' => $officeEmployees,
            ]);
    }

    public function search(){
        return view('item.itemFindPrivileged')->with(['results' => array(),
            'searched' => false]);
    }
    public function searched(Request $request){
        $required = array("", "", "", "", "", "");
        if($request->awb == null && $request->zip == null && $request->name == null && $request->street == null)
            $required = array('required|', 'required', 'required', '', '', 'required');
        if($request->street != null){
            $required[2] = $required[3] = $required[4] = 'required';
        }
        if($request->zip != null){
            $required[5] = 'required';
        }
        $data = $request->validate([
            'awb' => $required[0] . 'max:15',
            'name' => $required[1],
            'street' => $required[2],
            'city' => $required[3],
            'county' => $required[4],
            'zip' => $required[5]
        ]);

        $and = false;
        $query = "SELECT id FROM items WHERE ";
        if($data['awb'] != null){
            $awb = AWB::where('number', $data['awb'])->first();
            $query = $query . "awb_id = ". $awb->id;
            $and = true;
        }
        if($data['name'] != null){
            $name = explode(' ', $data['name']);
            if($and)
                $query = $query . ' AND ';
            $or = false;
            $and = true;
            foreach($name as $nam){
                if($or)
                    $query = $query . ' AND ';
                $query = $query . "(surname LIKE '" . $nam . "' OR name LIKE '" . $nam . "')";
                $or = true;
            }
        }
        $st = false;
        if($data['street'] != null){
            $st = true;
            $street = explode(' ', $data['street']);
            if($and)
                $query = $query . " AND ";
            $or = false;
            $and = true;
            foreach($street as $str){
                if($or)
                    $query = $query . " AND ";
                $or = true;
                $query = $query . "(street LIKE '" . $str . "' OR number LIKE '" . $str . "')";
            }
            $query = $query . " AND country_id = " . $request->country;
        }
        if($data['zip'] != null){
            if($and)
                $query = $query . " AND ";
            if(!$st)
                $query = $query . "country_id = " . $request->country . " AND ";
            $zip = ZipCode::where('zip', $data['zip'])->where('country_id', $request->country)->first();
            $query = $query . "zipcode_id = " . $zip->id;
        }
        $results = DB::select($query);
        $res = array();
        $i = 0;
        foreach($results as $resultId){
            array_push($res, Item::find($resultId->id));
        }
        return view('item.itemFindPrivileged')->with(['results' => $res,
            'searched' => true]);
    }
    public function modify($id){
        $result = Item::find($id);
        $nextLocation = new \stdClass();
        if($result->awb->next_location_type == 0){
            $hub = Hub::find($result->awb->next_location_id);
            $nextLocation->name = $hub->name;
            $nextLocation->street = $hub->street;
            $nextLocation->number = $hub->number;
            $nextLocation->block = null;
            $nextLocation->staircase =null;
            $nextLocation->apartment = null;
            $nextLocation->city = $hub->city;
            $nextLocation->county = $hub->county;
            $nextLocation->country = $hub->country->name;
            $nextLocation->zipcode = null;
        }else if($result->awb->next_location_type == 1){
            $office = Office::find($result->awb->next_location_id);
            $nextLocation->name = $office->name;
            $nextLocation->street = $office->street;
            $nextLocation->number = $office->number;
            $nextLocation->block = null;
            $nextLocation->staircase =null;
            $nextLocation->apartment = null;
            $nextLocation->city = $office->city;
            $nextLocation->county = $office->county;
            $nextLocation->country = $office->country->name;
            $nextLocation->zipcode = null;
        }else if($result->awb->next_location_type == 2){
            $nextLocation->name = $result->name . ' ' . $result->surname;
            $nextLocation->street = $result->street;
            $nextLocation->number = $result->number;
            $nextLocation->block = $result->block;
            $nextLocation->staircase = $result->staircase;
            $nextLocation->apartment = $result->apartment;
            $nextLocation->city = $result->city;
            $nextLocation->county = $result->county;
            $nextLocation->country = $result->country->name;
            $nextLocation->zipcode = $result->zipcode->zip;
        }
        return view('item.modifyItem')->with(['result' => $result,
            'nextLocation'=> $nextLocation]);
    }
    public function updateLocation($id){
        $item = Item::find($id);
        $awb = AWB::find($item->awb->id);
        if($awb->status2 == null)
            return redirect('/item/' . $id);
        if($item->awb->next_location_type == 0 && $item->awb->next_location_id == Auth::user()->office->hub->id){
            if($item->country->id != Auth::user()->office->hub->country->id && Auth::user()->office->hub->international == false){
                $awb->next_location_id = Hub::where('country_id', Auth::user()->office->hub->country->id)->where('international', true)->first()->id;
            }else if($item->country->id != Auth::user()->office->hub->country->id){
                $awb->next_location_id = Hub::where('country_id', $item->country->id)->where('international', true)->first()->id;
            }else if(Auth::user()->office->hub->id != $item->zipcode->office->hub->id){
                $awb->next_location_id = $item->zipcode->office->hub->id;
            }else{
                $awb->next_location_type = 1;
                $awb->next_location_id = Auth::user()->office->id;
            }
            $awb->locations = $awb->locations . '<br>' . Auth::user()->office->hub->name;
        }else if($item->awb->next_location_type == 0){
            $hub = Hub::find($item->awb->next_location_id);
            if($hub->country->id == $item->country->id && $hub->id == $item->zipcode->office->hub->id) {
                $awb->next_location_type = 1;
                $awb->next_location_id = $item->office->id;
            }else if($hub->country->id == $item->country->id){
                $awb->next_location_id = $item->office->hub->id;
            }else if($hub->country->id != $item->country->id && $hub->international == true){
                $awb->next_location_id = Hub::where('country_id', $item->country->id)->where('international', true)->first()->id;
            }else if($hub->country->id != $item->country->id){
                $awb->next_location_id  = Hub::where('country_id', $hub->country->id)->where('international', true)->first()->id;
            }
            $awb->locations = $awb->locations . '<br>' . $hub->name;
        }
        $awb->save();
        return redirect('/item/' . $id);
    }
    public function intransit($id){
        $item = Item::find($id);
        $awb = AWB::find($item->awb->id);
        $awb->status2 = Carbon::now()->toDateTimeString();
        $awb->save();
        return redirect('/item/' . $id);
    }

    public function outForDelivery($id){
        $item = Item::find($id);
        $awb = AWB::find($item->awb->id);
        $awb->status3 = Carbon::now()->toDateTimeString();
        $awb->next_location_type = 2;
        $awb->save();
        return redirect('/item/' . $id);
    }
    public function delivered($id){
        $item = Item::find($id);
        $awb = AWB::find($item->awb->id);
        $awb->status4 = Carbon::now()->toDateTimeString();
        $awb->save();
        return redirect('/item/' . $id);
    }
    protected function confirmDelete($id){
        return view('item.confirmDelete')->with('id', $id);
    }

    protected function delete($id){
        $item = Item::find($id);
        $awb = AWB::find($item->awb->id);
        $item->delete();
        $awb->delete();
        return redirect('/privilege-search');
    }

    public function create(){
        return view('item.createItem');
    }
    protected function registerItem(Request $request){
        $data = $request->validate([
            'name' => ['required', 'string', 'max:100'],
            'surname' => ['required', 'string', 'max:100'],
            'street' => ['required', 'string', 'max:100'],
            'number' => ['required', 'string', 'max:100'],
            'block' => ['nullable', 'string', 'max:100'],
            'staircase' => ['nullable', 'string', 'max:100'],
            'apartment' => ['nullable', 'string', 'max:100'],
            'city' => ['required', 'string', 'max:100'],
            'county' => ['required', 'string', 'max:100'],
            'country' => ['required', 'integer'],
            'zipcode' => ['required', 'string', 'max:100', Rule::exists('zip_codes', 'zip')->where('country_id', $request->country)],
            'weight' => ['numeric', 'nullable', 'min:0'],
            'volume1' => ['numeric', 'nullable', 'min:0'],
            'volume2' => ['numeric', 'nullable', 'min:0'],
            'volume3' => ['numeric', 'nullable', 'min:0'],
        ]);
        if($data['volume1'] != 0 || $data['volume2'] != 0 || $data['volume3'] != 0){
            if($data['volume1'] == 0)
                $data['volume1'] = 1;
            if($data['volume2'] == 0)
                $data['volume2'] = 1;
            if($data['volume3'] == 0)
                $data['volume3'] = 1;
        }
        $data['volume'] = $data['volume1'] * $data['volume2'] * $data['volume3'];
        $data['priority'] = $request->priority;
        $zipcode = ZipCode::where('country_id', $data['country'])->where('zip', $data['zipcode'])->first();
        if($zipcode == null)
            return redirect('/register-item');
        $awb = new AWB([
            'number' => Carbon::now()->day . Carbon::now()->month . Carbon::now()->year . Carbon::now()->hour . Carbon::now()->minute . Carbon::now()->second,
            'locations' => '',
            'status1' => Carbon::now()->toDateTimeString(),
            'next_location_type' => 0,
            'next_location_id' => Auth::user()->office->hub->id,
            'office_id' => $zipcode->office->id
        ]);
        $price = 0;
        if($data['volume'] == null && $data['weight'] != null)
            $data['volume'] = 0;
        if($data['weight'] == null && $data['volume'] != null)
            $data['weight'] = 0;
        if($data['priority'] != null){
            if($data['country'] == Auth::user()->office->country->id){
                if($data['volume'] == null && $data['weight'] == null || $data['volume'] == 0 && $data['weight'] == 0){
                    $price = Auth::user()->office->country->internal_letter_price_priority;
                }else{
                    if($data['volume'] * Auth::user()->office->country->internal_parcel_price_cmcub_priority >= $data['weight'] * Auth::user()->office->country->internal_parcel_price_kg_priority)
                        $price = $data['volume'] * Auth::user()->office->country->internal_parcel_price_cmcub_priority;
                    else $price = $data['weight'] * Auth::user()->office->country->internal_parcel_price_kg_priority;
                }
            }else{
                $fares = Fare::where('departure_continent', Auth::user()->office->country->continent->id)->where('arrival_continent', Country::find($data['country'])->continent->id)->first();
                if($data['volume'] == null && $data['weight'] == null || $data['volume'] == 0 && $data['weight'] == 0){
                    $price = $fares->external_letter_price_priority;
                }else{
                    if($data['volume'] * $fares->external_parcel_price_cmcub_priority >= $data['weight'] * $fares->external_parcel_price_kg_priority)
                        $price = $data['volume'] * $fares->external_parcel_price_cmcub_priority;
                    else $price = $data['weight'] * $fares->external_parcel_price_kg_priority;
                }
            }
        }else{
            if($data['country'] == Auth::user()->office->country->id){
                if($data['volume'] == null && $data['weight'] == null || $data['volume'] == 0 && $data['weight'] == 0){
                    $price = Auth::user()->office->country->internal_letter_price_economy;
                }else{
                    if($data['volume'] * Auth::user()->office->country->internal_parcel_price_cmcub_economy >= $data['weight'] * Auth::user()->office->country->internal_parcel_price_kg_economy)
                        $price = $data['volume'] * Auth::user()->office->country->internal_parcel_price_cmcub_economy;
                    else $price = $data['weight'] * Auth::user()->office->country->internal_parcel_price_kg_economy;
                }
            }else{
                $fares = Fare::where('departure_continent', Auth::user()->office->country->continent->id)->where('arrival_continent', Country::find($data['country'])->continent->id)->first();
                if($data['volume'] == null && $data['weight'] == null || $data['volume'] == 0 && $data['weight'] == 0){
                    $price = $fares->external_letter_price_economy;
                }else{
                    if($data['volume'] * $fares->external_parcel_price_cmcub_economy >= $data['weight'] * $fares->external_parcel_price_kg_economy)
                        $price = $data['volume'] * $fares->external_parcel_price_cmcub_economy;
                    else $price = $data['weight'] * $fares->external_parcel_price_kg_economy;
                }
            }
        }
        $awb->save();
        $item = new Item([
            'name' => $data['name'],
            'surname' => $data['surname'],
            'street' => $data['street'],
            'number' => $data['number'],
            'block' => $data['block'],
            'staircase' => $data['staircase'],
            'apartment' => $data['apartment'],
            'city' => $data['city'],
            'county' => $data['county'],
            'country_id' => $data['country'],
            'office_id' => $zipcode->office->id,
            'zipcode_id' => $zipcode->id,
            'awb_id' => $awb->id,
            'weight' => $data['weight'],
            'volume' => $data['volume'],
            'priority' => $data['priority'] != null,
            'price' => $price
        ]);

        $item->save();
        return redirect('/item-registered/' . $item->id);
    }
    protected function registeredItem($id){
        $item = Item::find($id);
        return view('item.newItemCompleted')->with('item', $item);
    }
    protected function deleteEmployee($id){
        $employee = User::find($id);
        if(Auth::user()->permission_level <= $employee->permission_level || Auth::user()->permission_level == 5)
            return redirect('/manager');
        return view('employee.deleteConfirmation')->with('employee', $employee);
    }
    protected function confirmDeleteEmployee($id){
        $employee = User::find($id);
        if(Auth::user()->permission_level <= $employee->permission_level || Auth::user()->permission_level == 5)
            return redirect('/manager');
        $employee->delete();
        return redirect('/manager');
    }
    public function editForm($id){
        $employee = User::find($id);
        if(Auth::user()->permission_level <= $employee->permission_level)
            return redirect('/manager');
        return view('employee.editEmployee')->with('employee', $employee);
    }
    public function editFormSubmit(Request $request, $id){
        $user = User::find($id);
        if(Auth::user()->permission_level <= $user->permission_level)
            return redirect('/manager');
        $data = $request->validate([
            'name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'surname' => ['required', 'string', 'max:100'],
            'street' => ['required', 'string', 'max:100'],
            'number' => ['required', 'string', 'max:100'],
            'block' => ['nullable', 'string', 'max:100'],
            'staircase' => ['nullable', 'string', 'max:100'],
            'apartment' => ['nullable', 'string', 'max:100'],
            'city' => ['required', 'string', 'max:100'],
            'county' => ['required', 'string', 'max:100'],
            'country' => ['required', 'integer'],
            'zipcode' => ['required', 'string', 'max:100'],
            'permissionlevel' => ['required', 'integer', 'max:7'],
            'office' => ['required', 'integer'],
        ]);
        $zip = ZipCode::where('country_id', $data['country'])->where('zip', $data['zipcode'])->first();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->surname = $data['surname'];
        $user->street = $data['street'];
        $user->number = $data['number'];
        $user->block = $data['block'];
        $user->staircase = $data['staircase'];
        $user->apartment = $data['apartment'];
        $user->city = $data['city'];
        $user->county = $data['county'];
        $user->country_id = $data['country'];
        $user->zipcode_id = $zip->id;
        $user->office_id = $data['office'];
        $user->permission_level = intval($data['permissionlevel']);
        $user->save();
        return redirect('/manager');
    }

    protected function employeeList($level){
        if($level >= Auth::user()->permission_level)
            return redirect('/manager');
        $users = null;
        if(Auth::user()->permission_level < 7)
            $users = User::where('permission_level', $level)->where('country_id', Auth::user()->country->id)->paginate(30);
        else $users = User::where('permission_level', $level)->paginate(30);
        $grade = "";
        switch($level){
            case 3:
                $grade = 'Local managers';
                break;
            case 4:
                $grade = 'Regional managers';
                break;
            case 5:
                $grade = 'National clerks';
                break;
            case 6:
                $grade = 'National managers';
                break;
            case 7:
                $grade = 'International managers';
                break;
            default: $grade = 'Office clerks';
        }
        return view('employee.employeeList')->with([
            'grade' => $grade,
            'employees' => $users
        ]);
    }

    public function officePage($id){
        $type = 'Office';
        $office = Office::find($id);
        $employees = User::where('office_id', $id)->orderBy('permission_level', 'DESC')->orderBy('surname', 'ASC')->orderBy('name', 'ASC')->paginate(20);
        return view('settlement')->with([
            'type' => $type,
            'settlement' => $office,
            'employees' => $employees
        ]);
    }

    public function hubPage($id){
        $type = 'Hub';
        $hub = Hub::find($id);
        $ids = array();
        foreach ($hub->offices as $office) {
            $employees = User::where('office_id', $office->id)->get();
            foreach($employees as $employee){
                array_push($ids, $employee->id);
            }
        }
        $employees = User::whereIn('id', $ids)->paginate(20);
        return view('settlement')->with([
            'type' => $type,
            'settlement' => $hub,
            'employees' => $employees
        ]);
    }

    public function internationalManagerEditInternalPrice(Request $request){
        $data = $request->validate([
            'country' => ['numeric', 'min:1']
        ]);
        return redirect('/manager/prices/internal' . $data['country']);
    }

    public function editInternalPrices($id){
        if($id != Auth::user()->office->country->id && Auth::user()->permission_level < 7)
            return redirect('/manager');
        $country = Country::find($id);
        return view('privilege.internalPriceManager')->with('country', $country);
    }

    public function editInternalPricesSubmit(Request $request, $id){
        if($id != Auth::user()->office->country->id && Auth::user()->permission_level < 7)
            return redirect('/manager');
        $data = $request->validate([
            'internal_letter_price_economy' => ['numeric', 'min:0'],
            'internal_parcel_price_kg_economy' => ['numeric', 'min:0'],
            'internal_parcel_price_cmcub_economy' => ['numeric', 'min:0'],
            'internal_letter_price_priority' => ['numeric', 'min:0'],
            'internal_parcel_price_kg_priority' => ['numeric', 'min:0'],
            'internal_parcel_price_cmcub_priority' => ['numeric', 'min:0'],
        ]);
        $country = Country::find($id);
        $country->internal_letter_price_economy = $data['internal_letter_price_economy'];
        $country->internal_parcel_price_kg_economy = $data['internal_parcel_price_kg_economy'];
        $country->internal_parcel_price_cmcub_economy = $data['internal_parcel_price_cmcub_economy'];
        $country->internal_letter_price_priority = $data['internal_letter_price_priority'];
        $country->internal_parcel_price_kg_priority = $data['internal_parcel_price_kg_priority'];
        $country->internal_parcel_price_cmcub_priority = $data['internal_parcel_price_cmcub_priority'];
        $country->save();
        return redirect('/manager');
    }
    protected function editExternalPrices(){
        $options = Continent::all();
        $fares = Fare::all();
        return view('privilege.externalPriceManager')->with([
            'options' => $options,
            'fares' => json_encode($fares)
        ]);
    }

    protected function editExternalPricesSubmit(Request $request){
        $data = $request->validate([
            'departure_continent' => ['numeric', 'max:6'],
            'arrival_continent' => ['numeric', 'max:6'],
            'external_letter_price_economy' => ['numeric', 'min:0'],
            'external_parcel_price_kg_economy' => ['numeric', 'min:0'],
            'external_parcel_price_cmcub_economy' => ['numeric', 'min:0'],
            'external_letter_price_priority' => ['numeric', 'min:0'],
            'external_parcel_price_kg_priority' => ['numeric', 'min:0'],
            'external_parcel_price_cmcub_priority' => ['numeric', 'min:0'],
        ]);
        $fare = Fare::where('departure_continent', $data['departure_continent'])->where('arrival_continent', $data['arrival_continent'])->first();
        $fare->external_letter_price_economy = $data['external_letter_price_economy'];
        $fare->external_parcel_price_kg_economy = $data['external_parcel_price_kg_economy'];
        $fare->external_parcel_price_cmcub_economy = $data['external_parcel_price_cmcub_economy'];
        $fare->external_letter_price_priority = $data['external_letter_price_priority'];
        $fare->external_parcel_price_kg_priority = $data['external_parcel_price_kg_priority'];
        $fare->external_parcel_price_cmcub_priority = $data['external_parcel_price_cmcub_priority'];
        $fare->save();
        return redirect(route('edit-external-prices'));
    }

    public function newNewsArticle(){
        return view('privilege.createNews');
    }
    protected function newNewsArticleSubmit(Request $request){
        $data = $request->validate([
            'subject' => ['string'],
            'news-article' => ['string']
        ]);
        $article = new News(['title' => $data['subject'],
            'content' => $data['news-article']]);
        $article->save();
        return redirect('/manager');
    }
    protected function contact(){
        $feedbacks = Feedback::orderBy('created_at', 'DESC')->paginate(30);
        return view('privilege.feedBackManager')->with('feedbacks', $feedbacks);
    }
}
