<?php

namespace App\Http\Controllers;

use App\AWB;
use App\Country;
use App\Fare;
use App\Item;
use App\News;
use App\Watch;
use App\ZipCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller
{
    public function news(){
        $newsList = News::orderBy('created_at', 'ASC')->simplePaginate(20);
        return view('news')->with('newsList', $newsList);
    }

    public function contact(){
        return view('contact');
    }
    public function search(){
        return view('item.itemFind')->with(['results' => array(),
                                                    'searched' => false,
                                                    'favourites' => array()]);
    }
    public function searched(Request $request){
        $required = array("", "", "", "", "", "");
        if($request->awb == null && $request->zip == null && $request->name == null && $request->street == null)
            $required = array('required|', 'required', 'required', '', '', 'required');
        if($request->street != null){
            $required[2] = $required[3] = $required[4] = 'required';
        }
        if($request->zip != null){
            $required[5] = 'required';
        }
        $data = $request->validate([
            'awb' => $required[0] . 'max:15',
            'name' => $required[1],
            'street' => $required[2],
            'city' => $required[3],
            'county' => $required[4],
            'zip' => $required[5]
        ]);

        $and = false;
        $query = "SELECT id FROM items WHERE ";
        if($data['awb'] != null){
            $awb = AWB::where('number', $data['awb'])->first();
            $query = $query . "awb_id = ". $awb->id;
            $and = true;
        }
        if($data['name'] != null){
            $name = explode(' ', $data['name']);
            if($and)
                $query = $query . ' AND ';
            $or = false;
            $and = true;
            foreach($name as $nam){
                if($or)
                    $query = $query . ' AND ';
                $query = $query . "(surname LIKE '" . $nam . "' OR name LIKE '" . $nam . "')";
                $or = true;
            }
        }
        $st = false;
        if($data['street'] != null){
            $st = true;
            $street = explode(' ', $data['street']);
            if($and)
                $query = $query . " AND ";
            $or = false;
            $and = true;
            foreach($street as $str){
                if($or)
                    $query = $query . " AND ";
                $or = true;
                $query = $query . "(street LIKE '" . $str . "' OR number LIKE '" . $str . "')";
            }
            $query = $query . " AND country_id = " . $request->country;
        }
        if($data['zip'] != null){
            if($and)
                $query = $query . " AND ";
            if(!$st)
                $query = $query . "country_id = " . $request->country . " AND ";
            $zip = ZipCode::where('zip', $data['zip'])->where('country_id', $request->country)->first();
            $query = $query . "zipcode_id = " . $zip->id;
        }
        $results = DB::select($query);
        $res = array();
        $favs = array();
        $i = 0;
        foreach($results as $resultId){
            array_push($res, Item::find($resultId->id));
            array_push($favs, Watch::where('user_id', Auth::id())->where('item_id', $resultId->id)->first());
        }
        return view('item.itemFind')->with(['results' => $res,
            'searched' => true,
            'favourites' => $favs,
            ]);
    }
    public function calculator(){
        return view('estimate')->with('options', Country::all());
    }
    public function calculate(Request $request){
        $price = 0;
        $data = array('volume' => $request->volume,
            'weight' => $request->weight,
            'priority' => null,
            'dep_country' => $request->departure,
            'arr_country' => $request->arrival,
            );
        if($request->internal == "1" && $request->international == "0")
            $data['arr_country'] = $data['dep_country'];
        if($request->priority != 0)
            $data['priority'] = 1;
        if($data['volume'] == null && $data['weight'] != null)
            $data['volume'] = 0;
        if($data['weight'] == null && $data['volume'] != null)
            $data['weight'] = 0;
        if($data['priority'] != null){
            if($data['arr_country'] == $data['dep_country']){
                if($data['volume'] == null && $data['weight'] == null || $data['volume'] == 0 && $data['weight'] == 0){
                    $price = Country::find($data['dep_country'])->internal_letter_price_priority;
                }else{
                    if($data['volume'] * Country::find($data['dep_country'])->internal_parcel_price_cmcub_priority >= $data['weight'] * Country::find($data['dep_country'])->internal_parcel_price_kg_priority)
                        $price = $data['volume'] * Country::find($data['dep_country'])->internal_parcel_price_cmcub_priority;
                    else $price = $data['weight'] * Country::find($data['dep_country'])->internal_parcel_price_kg_priority;
                }
            }else{
                $fares = Fare::where('departure_continent', Country::find($data['dep_country'])->continent->id)->where('arrival_continent', Country::find($data['arr_country'])->continent->id)->first();
                if($data['volume'] == null && $data['weight'] == null || $data['volume'] == 0 && $data['weight'] == 0){
                    $price = $fares->external_letter_price_priority;
                }else{
                    if($data['volume'] * $fares->external_parcel_price_cmcub_priority >= $data['weight'] * $fares->external_parcel_price_kg_priority)
                        $price = $data['volume'] * $fares->external_parcel_price_cmcub_priority;
                    else $price = $data['weight'] * $fares->external_parcel_price_kg_priority;
                }
            }
        }else{
            if($data['arr_country'] == $data['dep_country']){
                if($data['volume'] == null && $data['weight'] == null || $data['volume'] == 0 && $data['weight'] == 0){
                    $price = Country::find($data['dep_country'])->internal_letter_price_economy;
                }else{
                    if($data['volume'] * Country::find($data['dep_country'])->internal_parcel_price_cmcub_economy >= $data['weight'] * Country::find($data['dep_country'])->internal_parcel_price_kg_economy)
                        $price = $data['volume'] * Country::find($data['dep_country'])->internal_parcel_price_cmcub_economy;
                    else $price = $data['weight'] * Country::find($data['dep_country'])->internal_parcel_price_kg_economy;
                }
            }else{
                $fares = Fare::where('departure_continent', Country::find($data['dep_country'])->continent->id)->where('arrival_continent', Country::find($data['arr_country'])->continent->id)->first();
                if($data['volume'] == null && $data['weight'] == null || $data['volume'] == 0 && $data['weight'] == 0){
                    $price = $fares->external_letter_price_economy;
                }else{
                    if($data['volume'] * $fares->external_parcel_price_cmcub_economy >= $data['weight'] * $fares->external_parcel_price_kg_economy)
                        $price = $data['volume'] * $fares->external_parcel_price_cmcub_economy;
                    else $price = $data['weight'] * $fares->external_parcel_price_kg_economy;
                }
            }
        }
        return $price;
    }

    public function addFavourites(Request $request){
        $data = $request->validate([
            'id' => ['required', 'integer']
        ]);
        if(!Auth::check()){
            throw new \Exception('User not authenticated.');
        }
        $watch = new Watch([
            'user_id' => Auth::id(),
            'item_id' => $data['id'],
        ]);
        $watch->save();
        return $watch;
    }

    public function removeFavourites(Request $request){
        $data = $request->validate([
            'id' => ['required', 'integer']
        ]);
        if(!Auth::check()){
            throw new \Exception('User not authenticated.');
        }
        $watch = Watch::where('user_id', Auth::id())->where('item_id', $data['id'])->first();
        $watch->delete();
        return $data;
    }

    public function favourites(){
        $data = Watch::where('user_id', Auth::id())->get();
        $results = array();
        foreach($data as $piece){
            array_push($results, $piece->item);
        }
        return view('watches')->with('results', $results);
    }

    public function removeFavouritesList($id){
        $watch = Watch::where('item_id', $id)->where('user_id', Auth::id())->first();
        $watch->delete();
        return redirect('/favourites');
    }
}
