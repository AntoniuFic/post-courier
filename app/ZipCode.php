<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZipCode extends Model
{
    protected $guarded = [];

    public function office(){
        return $this->belongsTo('App\Office');
    }
    public function users(){
        return $this->hasMany('App\User');
    }
    public function items(){
        return $this->hasMany('App\Item');
    }
    public function country(){
        return $this->belongsTo('App\Country');
    }
}
