<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Office;

class AWB extends Model
{
    protected $fillable = [
        'number', 'locations', 'next_location_type', 'next_location_id', 'office_id', 'status1', 'status2', 'status3', 'status4'
    ];
    protected $table = 'awbs';
    public function item(){
        return $this->hasOne('App\Item');
    }
    public function office(){
        return $this->belongsTo('App\Office');
    }
    public function shipment(){
        return $this->belongsTo('App\Shipment');
    }
    public function nextLocation(){
        if($this->next_locaton_type == 0){
            $office = Office::find($this->next_location_id);
            return $office;
        }else if($this->next_location_type == 1){
            $hub = Hub::find($this->next_location_id);
            return $hub;
        }
        return null;
    }
}
