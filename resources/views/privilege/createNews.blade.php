@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">New article</div>
                    <form method="POST" action="{{ route('create-news') }}">
                        @csrf

                        <div>
                            <label for="subject" class="col-md-4 col-form-label">{{ __('Subject') }}</label>

                            <div class="col-md">
                                <input id="subject" type="text" class="form-control @error('subject') is-invalid @enderror" name="subject" value="{{ old('subjet') }}" required autocomplete="name" autofocus>

                                @error('subject')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            <label for="news-article" class="col-md-4 col-form-label">{{ __('Article') }}</label>

                            <div class="col-md">
                                <textarea id="news-article" style="width: 100%" name="news-article" min="20" max="1000"></textarea>
                                @error('news-article')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-4" style="margin: 30px;">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Upload') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
@endsection
