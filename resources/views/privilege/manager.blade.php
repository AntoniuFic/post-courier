@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if(Auth::user()->permission_level > 1)
                        <button class="accordion">Items</button>

                        <div class="card-body panel">
                            <a class="btn btn-outline-dark" href="{{ route('privilege-search') }}">Modify item</a>
                            <br><a class="btn btn-outline-dark" href="{{ route('register-item') }}">Register item</a>
                        </div>
                    @endif
                    @if(Auth::user()->permission_level > 2)
                            <button class="accordion">Employees</button>
                        @if(Auth::user()->permission_level == 3)
                            <div class="card-body panel">
                                @foreach($officeEmployees as $employee)
                                    <span>{{ $employee->name }} {{ $employee->surname }}</span>|<span>{{ $employee->email }}|<a href="/manager/employee/edit/{{$employee->id}}">Edit</a><a href="/manager/employee/delete/{{$employee->id}}">Delete</a></span><br>
                                @endforeach
                                @if($officeEmployees == null)
                                    <p>No employees at this facility.</p>
                                @endif
                                <a class="btn btn-outline-dark" href="/register-employee">Register employee</a>
                            </div>
                        @else
                            <div class="card-body panel">
                                <a class="btn btn-outline-dark" href="/register-employee">Register employee</a>
                                @if(Auth::user()->permission_level > 3)
                                    <a class="btn btn-outline-dark" href="/manager/employee/3">Local managers</a>
                                @endif
                                @if(Auth::user()->permission_level > 4)
                                    <br><a  class="btn btn-outline-dark"  href="/manager/employee/4">Regional managers</a>
                                @endif
                                @if(Auth::user()->permission_level > 5)
                                    <br><a class="btn btn-outline-dark"  href="/manager/employee/5">National clerks</a>
                                @endif
                                @if(Auth::user()->permission_level > 6)
                                    <br><a class="btn btn-outline-dark"  href="/manager/employee/6">National managers</a>
                                @endif
                            </div>
                        @endif
                    @endif
                    @if(Auth::user()->permission_level > 3)
                            <button class="accordion">News articles</button>
                        <div class="card-body panel">
                            <a class="btn btn-outline-dark"  href="{{ route('create-news') }}">New article</a>
                        </div>
                    @endif
                    @if(Auth::user()->permission_level > 4)
                            <button class="accordion">Feedbacks</button>
                            <div class="card-body panel">
                                <a class="btn btn-outline-dark"  href="{{ route('feedback-manager') }}">Feedback manager</a>
                            </div>
                            <button class="accordion">Prices</button>
                            <div class="card-body panel">
                                @if(Auth::user()->permission_level == 7)
                                    <form method="POST" action="/manager">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                                            <div class="col-md-6">
                                                <select id="country" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ old('country') }}" required autocomplete="country">
                                                    <?php $options = App\Country::all(); ?>
                                                    @foreach ($options as $option)
                                                        <option value="{{ $option -> id }}">{{ $option->name }}</option>
                                                    @endforeach
                                                </select>
                                                @error('country')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Edit') }}
                                                </button>
                                                <a class="btn btn-outline-dark" href="/manager/prices/external">External prices</a>
                                            </div>
                                        </div>
                                    </form>

                                @else
                                    <form method="POST" action="/manager/prices/internal/{{ $country->id }}">
                                        @csrf
                                        <div class="card-header">{{ $country->name }} price editor</div>

                                        <div class="form-group row">
                                            <label for="internal_letter_price_economy" class="col-md-4 col-form-label text-md-right">{{ __('Price for internal letter, economy package (price in EUR)') }}</label>

                                            <div class="col-md-6">
                                                <input id="internal_letter_price_economy" type="number" step="0.01" min="0" class="form-control @error('internal_letter_price_economy') is-invalid @enderror" name="internal_letter_price_economy" value="{{ old('internal_letter_price_economy') }}" required autocomplete="internal_letter_price_economy">

                                                @error('internal_letter_price_economy')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="internal_parcel_price_kg_economy" class="col-md-4 col-form-label text-md-right">{{ __('Price for internal parcel, economy package (EUR/KG)') }}</label>

                                            <div class="col-md-6">
                                                <input id="internal_parcel_price_kg_economy" type="number" step="0.01" min="0" class="form-control @error('internal_parcel_price_kg_economy') is-invalid @enderror" name="internal_parcel_price_kg_economy" value="{{ old('internal_parcel_price_kg_economy') }}" required autocomplete="internal_parcel_price_kg_economy">

                                                @error('internal_parcel_price_kg_economy')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="internal_parcel_price_cmcub_economy" class="col-md-4 col-form-label text-md-right">{{ __('Price for internal parcel, economy package (EUR/cube cm)') }}</label>

                                            <div class="col-md-6">
                                                <input id="internal_parcel_price_cmcub_economy" type="number" step="0.01" min="0" class="form-control @error('internal_parcel_price_cmcub_economy') is-invalid @enderror" name="internal_parcel_price_cmcub_economy" value="{{ old('internal_parcel_price_cmcub_economy') }}" required autocomplete="internal_parcel_price_cmcub_economy">

                                                @error('internal_parcel_price_cmcub_economy')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="internal_letter_price_priority" class="col-md-4 col-form-label text-md-right">{{ __('Price for internal letter, priority package (price in EUR)') }}</label>

                                            <div class="col-md-6">
                                                <input id="internal_letter_price_priority" type="number" step="0.01" min="0" class="form-control @error('internal_letter_price_priority') is-invalid @enderror" name="internal_letter_price_priority" value="{{ old('internal_letter_price_priority') }}" required autocomplete="internal_letter_price_priority">

                                                @error('internal_letter_price_priority')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="internal_parcel_price_kg_priority" class="col-md-4 col-form-label text-md-right">{{ __('Price for internal parcel, priority package (EUR/KG)') }}</label>

                                            <div class="col-md-6">
                                                <input id="internal_parcel_price_kg_priority" type="number" step="0.01" min="0" class="form-control @error('internal_parcel_price_kg_priority') is-invalid @enderror" name="internal_parcel_price_kg_priority" value="{{ old('internal_parcel_price_kg_priority') }}" required autocomplete="internal_parcel_price_kg_priority">

                                                @error('internal_parcel_price_kg_priority')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="internal_parcel_price_cmcub_priority" class="col-md-4 col-form-label text-md-right">{{ __('Price for internal parcel, priority package (EUR/cube cm)') }}</label>

                                            <div class="col-md-6">
                                                <input id="internal_parcel_price_cmcub_priority" type="number" step="0.01" min="0" class="form-control @error('internal_parcel_price_cmcub_priority') is-invalid @enderror" name="internal_parcel_price_cmcub_priority" value="{{ old('internal_parcel_price_cmcub_priority') }}" required autocomplete="internal_parcel_price_cmcub_priority">

                                                @error('internal_parcel_price_cmcub_priority')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Save') }}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
