@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <form id="form" method="POST" action="/manager/prices/external">
                        @csrf
                        <div class="card-header">Price editor</div>
                        <div>
                            <label for="departure_continent" class="col-md col-form-label">{{ __('Departure continent') }}</label>

                            <div class="col-md">

                                <select id="departure_continent" class="form-control @error('departure_continent') is-invalid @enderror" name="departure_continent" value="{{ old('departure_continent') }}"  autocomplete="departure_continent">
                                @foreach ($options as $option)
                                    <option value="{{ $option -> id }}">{{ $option->name }}</option>
                                @endforeach
                            </select>
                            @error('departure_continent')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        </div>
                        <div>
                            <label for="arrival_continent" class="col-md col-form-label">{{ __('Arrival continent') }}</label>

                            <div class="col-md">

                                <select id="arrival_continent" class="form-control @error('arrival_continent') is-invalid @enderror" name="arrival_continent" value="{{ old('arrival_continent') }}"  autocomplete="arrival_continent">
                                @foreach ($options as $option)
                                    <option value="{{ $option -> id }}">{{ $option->name }}</option>
                                @endforeach
                            </select>
                            @error('arrival_continent')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        </div>

                        <div>
                            <label for="external_letter_price_economy" class="col-md col-form-label">{{ __('Price for external letter, economy package (price in EUR)') }}</label>

                            <div class="col-md">
                                <input id="external_letter_price_economy" type="number" step="0.01" min="0" class="form-control @error('external_letter_price_economy') is-invalid @enderror" name="external_letter_price_economy" value="{{ old('external_letter_price_economy') }}" required autocomplete="external_letter_price_economy">

                                @error('external_letter_price_economy')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            <label for="external_parcel_price_kg_economy" class="col-md col-form-label">{{ __('Price for external parcel, economy package (EUR/KG)') }}</label>

                            <div class="col-md">
                                <input id="external_parcel_price_kg_economy" type="number" step="0.01" min="0" class="form-control @error('external_parcel_price_kg_economy') is-invalid @enderror" name="external_parcel_price_kg_economy" value="{{ old('external_parcel_price_kg_economy') }}" required autocomplete="external_parcel_price_kg_economy">

                                @error('external_parcel_price_kg_economy')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            <label for="external_parcel_price_cmcub_economy" class="col-md col-form-label">{{ __('Price for external parcel, economy package (EUR/cube cm)') }}</label>

                            <div class="col-md">
                                <input id="external_parcel_price_cmcub_economy" type="number" step="0.01" min="0" class="form-control @error('external_parcel_price_cmcub_economy') is-invalid @enderror" name="external_parcel_price_cmcub_economy" value="{{ old('external_parcel_price_cmcub_economy') }}" required autocomplete="external_parcel_price_cmcub_economy">

                                @error('external_parcel_price_cmcub_economy')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            <label for="external_letter_price_priority" class="col-md col-form-label">{{ __('Price for external letter, priority package (price in EUR)') }}</label>

                            <div class="col-md">
                                <input id="external_letter_price_priority" type="number" step="0.01" min="0" class="form-control @error('external_letter_price_priority') is-invalid @enderror" name="external_letter_price_priority" value="{{ old('external_letter_price_priority') }}" required autocomplete="external_letter_price_priority">

                                @error('external_letter_price_priority')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            <label for="external_parcel_price_kg_priority" class="col-md col-form-label">{{ __('Price for external parcel, priority package (EUR/KG)') }}</label>

                            <div class="col-md">
                                <input id="external_parcel_price_kg_priority" type="number" step="0.01" min="0" class="form-control @error('external_parcel_price_kg_priority') is-invalid @enderror" name="external_parcel_price_kg_priority" value="{{ old('external_parcel_price_kg_priority') }}" required autocomplete="external_parcel_price_kg_priority">

                                @error('external_parcel_price_kg_priority')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            <label for="external_parcel_price_cmcub_priority" class="col-md col-form-label">{{ __('Price for external parcel, priority package (EUR/cube cm)') }}</label>

                            <div class="col-md">
                                <input id="external_parcel_price_cmcub_priority" type="number" step="0.01" min="0" class="form-control @error('external_parcel_price_cmcub_priority') is-invalid @enderror" name="external_parcel_price_cmcub_priority" value="{{ old('external_parcel_price_cmcub_priority') }}" required autocomplete="external_parcel_price_cmcub_priority">

                                @error('external_parcel_price_cmcub_priority')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <div class="col-md offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        window.fares = {!! $fares !!};
    </script>
@endsection
