@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if(0 < $feedbacks->count())
                    @foreach ($feedbacks as $feedback)
                        <div class="card">
                            <div class="card-header">{{ $feedback->subject }}</div>

                            <div class="card-body">
                                {{ $feedback->user->name }} {{ $feedback->user->surname }} | {{ $feedback->user->email }}
                                {!! $feedback->content !!}
                            </div>
                        </div>
                    @endforeach
                    {{ $feedbacks->links() }}
                @else
                    <div class="card">
                        <div class="card-header">No feedbacks to display</div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
