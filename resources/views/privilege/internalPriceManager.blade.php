@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                  <form method="POST" action="/manager/prices/internal/{{ $country->id }}">
                      @csrf
                    <div class="card-header">{{ $country->name }} price editor</div>

                    <div>
                        <label for="internal_letter_price_economy" class="col-md col-form-label">{{ __('Price for internal letter, economy package (price in EUR)') }}</label>

                        <div class="col-md">
                            <input id="internal_letter_price_economy" type="number" step="0.01" min="0" class="form-control @error('internal_letter_price_economy') is-invalid @enderror" name="internal_letter_price_economy" value="{{ old('internal_letter_price_economy') }}" required autocomplete="internal_letter_price_economy">

                            @error('internal_letter_price_economy')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div>
                        <label for="internal_parcel_price_kg_economy" class="col-md col-form-label">{{ __('Price for internal parcel, economy package (EUR/KG)') }}</label>

                        <div class="col-md">
                            <input id="internal_parcel_price_kg_economy" type="number" step="0.01" min="0" class="form-control @error('internal_parcel_price_kg_economy') is-invalid @enderror" name="internal_parcel_price_kg_economy" value="{{ old('internal_parcel_price_kg_economy') }}" required autocomplete="internal_parcel_price_kg_economy">

                            @error('internal_parcel_price_kg_economy')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div>
                        <label for="internal_parcel_price_cmcub_economy" class="col-md col-form-label">{{ __('Price for internal parcel, economy package (EUR/cube cm)') }}</label>

                        <div class="col-md">
                            <input id="internal_parcel_price_cmcub_economy" type="number" step="0.01" min="0" class="form-control @error('internal_parcel_price_cmcub_economy') is-invalid @enderror" name="internal_parcel_price_cmcub_economy" value="{{ old('internal_parcel_price_cmcub_economy') }}" required autocomplete="internal_parcel_price_cmcub_economy">

                            @error('internal_parcel_price_cmcub_economy')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div>
                        <label for="internal_letter_price_priority" class="col-md col-form-label">{{ __('Price for internal letter, priority package (price in EUR)') }}</label>

                        <div class="col-md">
                            <input id="internal_letter_price_priority" type="number" step="0.01" min="0" class="form-control @error('internal_letter_price_priority') is-invalid @enderror" name="internal_letter_price_priority" value="{{ old('internal_letter_price_priority') }}" required autocomplete="internal_letter_price_priority">

                            @error('internal_letter_price_priority')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div>
                        <label for="internal_parcel_price_kg_priority" class="col-md col-form-label">{{ __('Price for internal parcel, priority package (EUR/KG)') }}</label>

                        <div class="col-md">
                            <input id="internal_parcel_price_kg_priority" type="number" step="0.01" min="0" class="form-control @error('internal_parcel_price_kg_priority') is-invalid @enderror" name="internal_parcel_price_kg_priority" value="{{ old('internal_parcel_price_kg_priority') }}" required autocomplete="internal_parcel_price_kg_priority">

                            @error('internal_parcel_price_kg_priority')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div>
                        <label for="internal_parcel_price_cmcub_priority" class="col-md col-form-label">{{ __('Price for internal parcel, priority package (EUR/cube cm)') }}</label>

                        <div class="col-md">
                            <input id="internal_parcel_price_cmcub_priority" type="number" step="0.01" min="0" class="form-control @error('internal_parcel_price_cmcub_priority') is-invalid @enderror" name="internal_parcel_price_cmcub_priority" value="{{ old('internal_parcel_price_cmcub_priority') }}" required autocomplete="internal_parcel_price_cmcub_priority">

                            @error('internal_parcel_price_cmcub_priority')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row mb-4">
                        <div class="col-md offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>
                  </form>
                </div>
                </div>
            </div>
        </div>
@endsection
