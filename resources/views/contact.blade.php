@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Contact information</div>

                    <div class="card-body">
                        Phone number: 0712 345 678<br>
                        International phone number: 0040 712 345 678<br>
                        E-mail: support@post.com<br>
                        Working hours: 08:00 - 16:00, M - F
                    </div>
                </div>

                @auth
                    <div class="card">
                        <div class="card-header">{{ __('Submit feedback') }}</div>

                        <div class="card-body">
                            <form method="POST" name="feedback" id="feedback" action="{{ route('contact') }}">
                                @csrf


                                <div>
                                    <label for="subject" class="col-md col-form-label">{{ __('Subject') }}</label>

                                    <div class="col-md">
                                        <input id="subject" type="text" class="form-control @error('subject') is-invalid @enderror" name="subject" required autocomplete="subject">

                                        @error('subject')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>

                                <div>
                                    <label for="contents" class="col-md col-form-label">{{ __('Content') }}</label>

                                    <div class="col-md">
                                        <textarea id="contents" rows="10" form="feedback" type="text" class="form-control @error('contents') is-invalid @enderror" name="contents" required autocomplete="contents">
                                        </textarea>
                                        @error('contents')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="form-group row mb-4">
                                    <div class="col-md-8 offset-md-4">
                                        <input type="submit" class="btn btn-primary" value="Submit">
                                    </div>
                                </div>
                            </form>
                            @endauth
            </div>
        </div>
    </div>
@endsection
@section('scripts')
                <script type="text/javascript" src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
@endsection
