@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
                <div class="card">
                    <div class="card-header">Calculate cost</div>

                    <div class="card-body">
                        <div class="tab">
                            <button id="but2" class="tablinks activated">International</button>
                            <button id="but1" class="tablinks">Internal</button>
                        </div>
                        <form method="GET" action="" onsubmit="false">
                            @csrf
                        <div>

                            <div class="col-md">

                                <input type="radio" name="international" id="international1" checked value="1" hidden>
                                <input type="radio" name="international" id="international0" value="0" hidden>
                                @error('international')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            <label for="departure_country" class="col-md col-form-label">{{ __('Departure country') }}</label>

                            <div class="col-md">

                                <select id="departure_country" class="form-control @error('departure_country') is-invalid @enderror" name="departure_country" value="{{ old('departure_country') }}"  autocomplete="departure_country">
                                    @foreach ($options as $option)
                                        <option value="{{ $option -> id }}">{{ $option->name }}</option>
                                    @endforeach
                                </select>
                                @error('departure_country')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div id="arrival_country_div">
                            <label for="arrival_country" class="col-md col-form-label">{{ __('Arrival country') }}</label>

                            <div class="col-md">

                                <select id="arrival_country" class="form-control @error('arrival_country') is-invalid @enderror" name="arrival_country" value="{{ old('arrival_country') }}"  autocomplete="arrival_country">
                                    @foreach ($options as $option)
                                        <option value="{{ $option -> id }}">{{ $option->name }}</option>
                                    @endforeach
                                </select>
                                @error('arrival_country')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div>
                            <label for="weight" class="col-md col-form-label">{{ __('Weight in kg(leave empty for letter)') }}</label>

                            <div class="col-md">
                                <input id="weight" type="text" class="form-control @error('weight') is-invalid @enderror" name="weight" value="{{ old('weight') }}" autocomplete="weight">

                                <span class="invalid">
                                        <strong id="weightMessage"></strong>
                                </span>
                            </div>
                        </div>
                        <div>
                            <label for="volume" class="col-md col-form-label">{{ __('Volume in cube cm(leave empty for letter)') }}</label>

                            <div class="col-md">
                                <input id="volume" type="text" class="form-control @error('volume') is-invalid @enderror" name="volume" value="{{ old('volume') }}" autocomplete="volume">

                                <span class="invalid">
                                        <strong id="volumeMessage"></strong>
                                    </span>
                            </div>
                        </div>
                        <div>
                            <div class="col-md offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="priority" id="priority" {{ old('priority') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="priority">
                                        {{ __('Priority') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        </form>
                        <div class="form-group row mb-0">
                            <div class="col-md offset-md-4">
                                <button id="button" class="btn btn-primary">
                                    {{ __('Calculate') }}
                                </button>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md offset-md-4">
                                <p id="price" hidden></p>
                            </div>
                        </div>
                </div>
    </div>
        </div>
    </div>
@endsection
