@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                @if(0 < $newsList->count())
                    @foreach ($newsList as $newsItem)
                        <div class="card">
                            <div class="card-header">{{ $newsItem->title }}</div>

                            <div class="card-body">
                                {!! $newsItem->content !!}
                            </div>
                        </div>
                    @endforeach
                    {{ $newsList->links() }}
                @else
                    <div class="card">
                        <div class="card-header">No news to display</div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
