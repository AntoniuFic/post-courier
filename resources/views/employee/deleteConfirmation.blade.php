@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Confirm delete</div>

                    <div class="card-body">
                        Are you sure you want to delete {{ $employee->name }} {{ $employee->surname  }}?
                        <form method="POST" action="/manager/employee/delete/{{$employee->id}}">
                            @csrf
                            <input type="submit" value="Delete">
                        </form>
                        <a href="/item/{{ $id }}">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
