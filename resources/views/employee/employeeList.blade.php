@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $grade }}</div>

                    <div class="card-body">
                        @if($employees->count() == 0)
                            No {{ $grade }} in the system.
                        @endif
                        @foreach($employees as $employee)
                            <span>{{ $employee->name }} {{ $employee->surname }} | {{ $employee->email }} | @if($employee->permission_level != 4)<a href="/manager/office/{{$employee->office->id}}">{{ $employee->office->name }}</a> @else <a href="/manager/hub/{{$employee->office->hub->id}}">{{ $employee->office->hub->name }}</a> @endif</span><a href="/manager/employee/edit/{{ $employee->id }}">Edit</a>@if(Auth::user()->permission_level != 5) <a href="/manager/employee/delete/{{$employee->id}}">Delete</a> @endif<br>
                        @endforeach
                        {{ $employees->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
