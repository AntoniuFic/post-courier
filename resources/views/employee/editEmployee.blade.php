@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Edit') }}</div>

                    <div class="card-body">
                        <form method="POST" action="/manager/employee/edit/{{ $employee->id }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $employee->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('Surname') }}</label>

                                <div class="col-md-6">
                                    <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ $employee->surname }}" required autocomplete="surname">

                                    @error('surname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $employee->email  }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="street" class="col-md-4 col-form-label text-md-right">{{ __('Street') }}</label>

                                <div class="col-md-6">
                                    <input id="street" type="text" class="form-control @error('street') is-invalid @enderror" name="street" value="{{ $employee->street }}" required autocomplete="street">

                                    @error('street')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Number') }}</label>

                                <div class="col-md-6">
                                    <input id="number" type="text" class="form-control @error('number') is-invalid @enderror" name="number" value="{{ $employee->number }}" required autocomplete="number">

                                    @error('number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="block" class="col-md-4 col-form-label text-md-right">{{ __('Block') }}</label>

                                <div class="col-md-6">
                                    <input id="block" type="text" class="form-control @error('block') is-invalid @enderror" name="block" value="@if($employee->block != null){{ $employee->block }} @endif" autocomplete="block">

                                    @error('block')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="staircase" class="col-md-4 col-form-label text-md-right">{{ __('Staircase') }}</label>

                                <div class="col-md-6">
                                    <input id="staircase" type="text" class="form-control @error('staircase') is-invalid @enderror" name="staircase" value="@if($employee->staircase != null) {{ $employee->staircase }} @endif" autocomplete="staircase">

                                    @error('staircase')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="apartment" class="col-md-4 col-form-label text-md-right">{{ __('Apartment') }}</label>

                                <div class="col-md-6">
                                    <input id="apartment" type="text" class="form-control @error('apartment') is-invalid @enderror" name="apartment" value="@if($employee->apartment != null) {{ $employee->apartment }} @endif" autocomplete="apartment">

                                    @error('apartment')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('City/Town/Village') }}</label>

                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ $employee->city }}" required autocomplete="city">

                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="county" class="col-md-4 col-form-label text-md-right">{{ __('County/State') }}</label>

                                <div class="col-md-6">
                                    <input id="county" type="text" class="form-control @error('county') is-invalid @enderror" name="county" value="{{ $employee->county }}" required autocomplete="county">

                                    @error('county')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                                <div class="col-md-6">
                                    <select id="country" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ $employee->country->id }}"  autocomplete="country">
                                        <?php $options = App\Country::all(); ?>
                                        @foreach ($options as $option)
                                            <option value="{{ $option -> id }}">{{ $option->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="zipcode" class="col-md-4 col-form-label text-md-right">{{ __('Zip code') }}</label>

                                <div class="col-md-6">
                                    <input id="zipcode" type="text" class="form-control @error('zipcode') is-invalid @enderror" name="zipcode" value="{{ $employee->zipcode->zip }}" required autocomplete="zipcode">

                                    @error('zipcode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="office" class="col-md-4 col-form-label text-md-right">{{ __('Office') }}</label>

                                <div class="col-md-6">
                                    <select id="office" class="form-control @error('office') is-invalid @enderror" name="office" value="{{ $employee->office->id }}"  autocomplete="office">
                                        <?php $options = App\Office::all(); ?>
                                        @foreach ($options as $option)
                                            <option value="{{ $option -> id }}">{{ $option->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('office')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="permissionlevel" class="col-md-4 col-form-label text-md-right">{{ __('Permission level') }}</label>

                                <div class="col-md-6">
                                    <select id="permissionlevel" class="form-control @error('permissionlevel') is-invalid @enderror" name="permissionlevel" value="{{ $employee->permission_level  }}"  autocomplete="permissionlevel">
                                        <?php $text = array("Office clerk/Worker", "Local manager", "Regional Manager", "National clerk", "National manager", "International manager")?>
                                        @for ($i = 2; $i < Auth::user()->permission_level; $i++)
                                            <option value="{{ $i }}">{{ $text[$i - 2] }}</option>
                                        @endfor
                                    </select>
                                    @error('permissionlevel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
