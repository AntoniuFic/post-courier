@extends('layouts.app')

@section('content')
    @if(count($results) == 0)
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card-body">
                    <div class="card-header">Favourite items</div>

                    <div class="card-body">
                        No favourite items.
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
<div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                @foreach($results as $result)
                        <div class="card-header">{{ $result->awb->number }}</div>
                    <div class="card-body">
                        Delivery for: {{ $result->name }} {{ $result->surname }} <br>
                        Destination: <div>Street {{ $result->street }}, No. {{ $result->number }}<br>
                            @if($result->block != null)
                                Block {{ $result->block }} <br>
                            @endif
                            @if($result->staircase != null)
                                Staircase {{ $result->staircase }} <br>
                            @endif
                            @if($result->apartment != null)
                                Apartment {{ $result->apartment }} <br>
                            @endif
                            City {{ $result->city }} <br>
                            County {{ $result->county }} <br>
                            Country {{ $result->country->name }} <br>
                            Zip code {{ $result->zipcode->zip }} <br>
                        </div>
                        Weight: @if($result->weight > 0) {{ $result->weight }} @else Letter @endif <br>
                        Volume: @if($result->volume > 0) {{ $result->volume }} @else Letter @endif <br>
                        Price: {{ $result->price }} EUR <br>
                        Priority: @if($result->priority) Yes @else No @endif <br>
                        <?php
                        $color[0] = $color[1] = $color[2] = $color[3] = 'gray';
                        $messages = array("", "", "", "");
                        $locations = '';
                        if($result->awb->status1 != null){
                            $color[0] = 'red';
                            $messages[0] = 'The item was dispatched by the office at: ' . $result->awb->status1;
                        }
                        if($result->awb->status2 != null){
                            $color[1] = 'red';
                            $messages[1] = 'The item is in transit since: ' . $result->awb->status2;
                            $locations = $result->awb->locations;
                        }
                        if($result->awb->status3 != null){
                            $color[2] = 'red';
                            $messages[2] = 'The item is out for delivery since: ' . $result->awb->status3;
                        }
                        if($result->awb->status4 != null){
                            $color[3] = 'red';
                            $messages[3] = 'The item was delivered at: ' . $result->awb->status4;
                        }

                        ?>
                        Status:<br>
                        <div style="padding: 10px 14px; border-radius: 20px; background-color: {{ $color[0] }}; text-align: center; display: inline; margin: 30px;"><span style="font-family: 'Nunito', sans-serif">1</span></div><div style="display: inline-block">{{ $messages[0] }}</div><br><br>
                        <div style="padding: 10px 14px; border-radius: 20px; background-color: {{ $color[1] }}; text-align: center; display: inline; margin: 30px;"><span style="font-family: 'Nunito', sans-serif">2</span></div><div style="display: inline-block">{{ $messages[1] }}</div> <div style="display: inline-block">{!! $locations !!}</div><br><br>
                        <div style="padding: 10px 14px; border-radius: 20px; background-color: {{ $color[2] }}; text-align: center; display: inline; margin: 30px;"><span style="font-family: 'Nunito', sans-serif">3</span></div><div style="display: inline-block">{{ $messages[2] }}</div><br><br>
                        <div style="padding: 10px 14px; border-radius: 20px; background-color: {{ $color[3] }}; text-align: center; display: inline; margin: 30px;"><span style="font-family: 'Nunito', sans-serif">4</span></div><div style="display: inline-block">{{ $messages[3] }} <?php if($result->awb->status4 != null) echo $result->office->name; ?></div><br><br>

                        <a class="btn btn-warning" href="/remove-fav/{{ $result->id }}">Remove from favourites</a>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
