@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $type }}: {{ $settlement->name }}</div>

                    <div class="card-body">
                            Address: <div>Street {{ $settlement->street }}, No. {{ $settlement->number }}<br>
                            City {{ $settlement->city }}<br>
                            County {{ $settlement->county }}<br>
                            Country {{ $settlement->country->name }}<br>
                        </div>
                        @if($type == 'Hub')
                            International: @if($settlement->international) Yes @else No @endif <br>
                        @endif
                        @foreach($employees as $employee)
                            <span>{{ $employee->name }} {{ $employee->surname }} | {{ $employee->email }}</span><a href="@if(!($employee->permission_level >= Auth::user()->permission_level)) /manager/employee/edit/{{ $employee->id }} @endif">Edit</a> <a href="@if(!(Auth::user()->permission_level != 5 && Auth::user()->permission_level <= $employee->permission_level)) /manager/employee/delete/{{$employee->id}} @endif">Delete</a><br>
                        @endforeach
                        {{ $employees->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
