@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Register item') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('register-item') }}">
                            @csrf

                            <div>
                                <label for="name" class="col-md col-form-label">{{ __('Name') }}</label>

                                <div class="col-md">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="surname" class="col-md col-form-label">{{ __('Surname') }}</label>

                                <div class="col-md">
                                    <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname') }}" required autocomplete="surname">

                                    @error('surname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="street" class="col-md col-form-label">{{ __('Street') }}</label>

                                <div class="col-md">
                                    <input id="street" type="text" class="form-control @error('street') is-invalid @enderror" name="street" value="{{ old('street') }}" required autocomplete="street">

                                    @error('street')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="number" class="col-md col-form-label">{{ __('Number') }}</label>

                                <div class="col-md">
                                    <input id="number" type="text" class="form-control @error('number') is-invalid @enderror" name="number" value="{{ old('number') }}" required autocomplete="number">

                                    @error('number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="block" class="col-md col-form-label">{{ __('Block') }}</label>

                                <div class="col-md">
                                    <input id="block" type="text" class="form-control @error('block') is-invalid @enderror" name="block" value="{{ old('block') }}" autocomplete="block">

                                    @error('block')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="staircase" class="col-md col-form-label">{{ __('Staircase') }}</label>

                                <div class="col-md">
                                    <input id="staircase" type="text" class="form-control @error('staircase') is-invalid @enderror" name="staircase" value="{{ old('staircase') }}" autocomplete="staircase">

                                    @error('staircase')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div>
                                <label for="apartment" class="col-md col-form-label">{{ __('Apartment') }}</label>

                                <div class="col-md">
                                    <input id="apartment" type="text" class="form-control @error('apartment') is-invalid @enderror" name="apartment" value="{{ old('apartment') }}" autocomplete="apartment">

                                    @error('apartment')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="city" class="col-md col-form-label">{{ __('City/Town/Village') }}</label>

                                <div class="col-md">
                                    <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" required autocomplete="city">

                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="county" class="col-md col-form-label">{{ __('County/State') }}</label>

                                <div class="col-md">
                                    <input id="county" type="text" class="form-control @error('county') is-invalid @enderror" name="county" value="{{ old('county') }}" required autocomplete="county">

                                    @error('county')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="country" class="col-md col-form-label">{{ __('Country') }}</label>

                                <div class="col-md">
                                    <select id="country" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ old('country') }}"  autocomplete="country">
                                        <?php $options = App\Country::all(); ?>
                                        @foreach ($options as $option)
                                            <option value="{{ $option -> id }}">{{ $option->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="zipcode" class="col-md col-form-label">{{ __('Zip code') }}</label>

                                <div class="col-md">
                                    <input id="zipcode" type="text" class="form-control @error('zipcode') is-invalid @enderror" name="zipcode" value="{{ old('zipcode') }}" required autocomplete="zipcode">

                                    @error('zipcode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="weight" class="col-md col-form-label">{{ __('Weight in kg') }}</label>

                                <div class="col-md">
                                    <input id="weight" type="number" step="0.001" min="0" class="form-control @error('weight') is-invalid @enderror" name="weight" value="{{ old('weight') }}" autocomplete="weight">

                                    @error('weight')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="volume1" class="col-md col-form-label">{{ __('Length in cm') }}</label>

                                <div class="col-md">
                                    <input id="volume1" type="number" step="0.001" min="0" class="form-control @error('volume1') is-invalid @enderror" name="volume1" value="{{ old('volume1') }}" autocomplete="volume1">

                                    @error('volume1')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="volume2" class="col-md col-form-label">{{ __('Width in cm') }}</label>

                                <div class="col-md">
                                    <input id="volume2" type="number" step="0.001" min="0" class="form-control @error('volume2') is-invalid @enderror" name="volume2" value="{{ old('volume2') }}" autocomplete="volume2">

                                    @error('volume')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <label for="volume3" class="col-md col-form-label">{{ __('Height in cm') }}</label>

                                <div class="col-md">
                                    <input id="volume3" type="number" step="0.001" min="0" class="form-control @error('volume3') is-invalid @enderror" name="volume3" value="{{ old('volume3') }}" autocomplete="volume3">

                                    @error('volume3')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div>
                                <div class="col-md offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="priority" id="priority" {{ old('priority') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="priority">
                                            {{ __('Priority') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-4">
                                <div class="col-md offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
