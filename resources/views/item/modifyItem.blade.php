@extends('layouts.app')

@section('content')
    <div class="black_overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Item</div>

                    <div class="card-body">
                        Delivery for: {{ $result->name }} {{ $result->surname }} <br>
                        Destination: <div>Street {{ $result->street }}, No. {{ $result->number }}<br>
                            @if($result->block != null)
                                Block {{ $result->block }} <br>
                            @endif
                            @if($result->staircase != null)
                                Staircase {{ $result->staircase }} <br>
                            @endif
                            @if($result->apartment != null)
                                Apartment {{ $result->apartment }} <br>
                            @endif
                            City {{ $result->city }} <br>
                            County {{ $result->county }} <br>
                            Country {{ $result->country->name }} <br>
                            Zip code {{ $result->zipcode->zip }} <br>
                            Weight: @if($result->weight > 0) {{ $result->weight }} @else Letter @endif <br>
                            Volume: @if($result->volume > 0) {{ $result->volume }} @else Letter @endif <br>
                            Price: {{ $result->price }} EUR <br>
                            Priority: @if($result->priority) Yes @else No @endif <br>
                        </div>
                        <?php
                        $color[0] = $color[1] = $color[2] = $color[3] = 'gray';
                        $messages = array("", "", "", "");
                        $locations = '';
                        if($result->awb->status1 != null){
                            $color[0] = 'red';
                            $messages[0] = 'The item was dispatched by the office at: ' . $result->awb->status1;
                        }
                        if($result->awb->status2 != null){
                            $color[1] = 'red';
                            $messages[1] = 'The item is in transit since: ' . $result->awb->status2;
                            $locations = $result->awb->locations;
                        }
                        if($result->awb->status3 != null){
                            $color[2] = 'red';
                            $messages[2] = 'The item is out for delivery since: ' . $result->awb->status3;
                        }
                        if($result->awb->status4 != null){
                            $color[3] = 'red';
                            $messages[3] = 'The item was delivered at: ' . $result->awb->status4;
                        }

                        ?>
                        Status:<br>
                        <div style="padding: 10px 14px; border-radius: 20px; background-color: {{ $color[0] }}; text-align: center; display: inline; margin: 30px;"><span style="font-family: 'Nunito', sans-serif">1</span></div><div style="display: inline-block">{{ $messages[0] }}</div><br><br>
                        <div style="padding: 10px 14px; border-radius: 20px; background-color: {{ $color[1] }}; text-align: center; display: inline; margin: 30px;"><span style="font-family: 'Nunito', sans-serif">2</span></div><div style="display: inline-block">{{ $messages[1] }}</div> <div style="display: inline-block">{!! $locations !!}</div><br><br>
                        <div style="padding: 10px 14px; border-radius: 20px; background-color: {{ $color[2] }}; text-align: center; display: inline; margin: 30px;"><span style="font-family: 'Nunito', sans-serif">3</span></div><div style="display: inline-block">{{ $messages[2] }}</div><br><br>
                        <div style="padding: 10px 14px; border-radius: 20px; background-color: {{ $color[3] }}; text-align: center; display: inline; margin: 30px;"><span style="font-family: 'Nunito', sans-serif">4</span></div><div style="display: inline-block">{{ $messages[3] }} <?php if($result->awb->status4 != null) echo $result->office->name; ?></div><br><br>

                        Next location:  <div>@if($nextLocation->name != null) {{ $nextLocation->name }} <br> @endif
                            Street {{ $nextLocation->street }}, No. {{ $nextLocation->number }}<br>
                            @if($nextLocation->block != null)
                                Block {{ $nextLocation->block }} <br>
                            @endif
                            @if($nextLocation->staircase != null)
                                Staircase {{ $nextLocation->staircase }} <br>
                            @endif
                            @if($nextLocation->apartment != null)
                                Apartment {{ $nextLocation->apartment }} <br>
                            @endif
                            City {{ $nextLocation->city }} <br>
                            County {{ $nextLocation->county }} <br>
                            Country {{ $nextLocation->country }} <br>
                            @if($nextLocation->zipcode != null)
                                Zip code {{ $nextLocation->zipcode }} <br>
                            @endif
                        </div>

                        <a href="/item/{{ $result->id }}/update-location" class="btn btn-warning" disabled="{{ $result->awb->status3 == null && $result->awb->status2 != null }}">Update location</a> <button id="myBtn" class="btn btn-dark" onclick="document.getElementById('myModal').style.display = 'block'; document.body.style.overflow = 'hidden';">Update status</button> <a class="btn btn-info" href="/item/{{ $result->id }}/delete">Delete entry</a>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('popup')

    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close" onclick="document.getElementById('myModal').style.display = 'none'; document.body.style.overflow = 'auto';">&times;</span>
            <a class="btn btn-info" style="margin: 20px;" href="/item/{{ $result->id }}/in-transit" disabled="{{ $result->awb->status2 != null }}">In transit</a>
            <a class="btn btn-info" style="margin: 20px;" href="/item/{{ $result->id }}/out-for-delivery" disabled="{{ $result->awb->status3 != null }}">Out for delivery</a>
            <a class="btn btn-info" style="margin: 20px;" href="/item/{{ $result->id }}/delivered" disabled="{{ $result->awb->status4 != null }}">Delivered</a>
        </div>

    </div>
@endsection
