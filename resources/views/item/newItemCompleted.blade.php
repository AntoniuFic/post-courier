@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Item registration completed</div>

                    <div class="card-body">
                        Tracking number: {{ $item->awb->number }} <br>
                        Reciever: {{ $item->name }} {{ $item->surname }}<br>
                        Destination: <div>Street {{ $item->street }}, No. {{ $item->number }}<br>
                            @if($item->block != null)
                                Block {{ $item->block }} <br>
                            @endif
                            @if($item->staircase != null)
                                Staircase {{ $item->staircase }} <br>
                            @endif
                            @if($item->apartment != null)
                                Apartment {{ $item->apartment }} <br>
                            @endif
                            City {{ $item->city }} <br>
                            County {{ $item->county }} <br>
                            Country {{ $item->country->name }} <br>
                            Zip code {{ $item->zipcode->zip }} <br>
                            Weight: @if($item->weight > 0) {{ $item->weight }} @else Letter @endif <br>
                            Volume: @if($item->volume > 0) {{ $item->volume }} @else Letter @endif <br>
                            Price: {{ $item->price }} EUR <br>
                            Priority: @if($item->priority) Yes @else No @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
