@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Item search</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('privilege-search') }}">
                            @csrf
                            <div>
                                <label for="awb" class="col-md col-form-label">{{ __('Tracking number') }}</label>

                                <div class="col-md">
                                    <input id="awb" type="text" class="form-control @error('awb') is-invalid @enderror" name="awb" value="{{ old('awb') }}" autocomplete="awb" autofocus>

                                    @error('awb')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div>
                                <label for="name" class="col-md col-form-label">{{ __('Reciever name and surname(separated by a space)') }}</label>

                                <div class="col-md">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div>
                                <label for="street" class="col-md col-form-label">{{ __('Street and number(separated by a space)') }}</label>

                                <div class="col-md">
                                    <input id="street" type="text" class="form-control @error('street') is-invalid @enderror" name="street" value="{{ old('street') }}" autocomplete="street" autofocus>

                                    @error('street')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div>
                                <label for="city" class="col-md col-form-label">{{ __('City') }}</label>

                                <div class="col-md">
                                    <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" autocomplete="city" autofocus>

                                    @error('city')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div>
                                <label for="county" class="col-md col-form-label">{{ __('County') }}</label>

                                <div class="col-md">
                                    <input id="county" type="text" class="form-control @error('county') is-invalid @enderror" name="county" value="{{ old('county') }}" autocomplete="county" autofocus>

                                    @error('county')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div>
                                <label for="country" class="col-md col-form-label">{{ __('Country') }}</label>

                                <div class="col-md">
                                    <select id="country" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ old('country') }}"  autocomplete="country">
                                        <?php $options = App\Country::all(); ?>
                                        @foreach ($options as $option)
                                            <option value="{{ $option -> id }}">{{ $option->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('country')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div>
                                <label for="zip" class="col-md col-form-label">{{ __('Zip code') }}</label>

                                <div class="col-md">
                                    <input id="zip" type="text" class="form-control @error('zip') is-invalid @enderror" name="zip" value="{{ old('zip') }}" autocomplete="zip" autofocus>

                                    @error('zip')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-3">
                                <div class="col-md offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Search') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
        @if(count($results) > 0)
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Results</div>
                        @foreach($results as $result)
                            <div class="card-body"  style="border: 1px solid #aaaaaa; margin: 5px; border-radius: 5px">
                                Delivery for: {{ $result->name }} {{ $result->surname }} <br>
                                Destination: <div>Street {{ $result->street }}, No. {{ $result->number }}<br>
                                    @if($result->block != null)
                                        Block {{ $result->block }}<br>
                                    @endif
                                    @if($result->staircase != null)
                                        Staircase {{ $result->staircase }}<br>
                                    @endif
                                    @if($result->apartment != null)
                                        Apartment {{ $result->apartment }}<br>
                                    @endif
                                    City {{ $result->city }}<br>
                                    County {{ $result->county }}<br>
                                    Country {{ $result->country->name }}<br>
                                    Zip code {{ $result->zipcode->zip }}<br>
                                    Weight: @if($result->weight > 0) {{ $result->weight }} @else Letter @endif <br>
                                    Volume: @if($result->volume > 0) {{ $result->volume }} @else Letter @endif <br>
                                    Price: {{ $result->price }} EUR <br>
                                    Priority: @if($result->priority) Yes @else No @endif <br>
                                </div>
                                <?php
                                $color[0] = $color[1] = $color[2] = $color[3] = 'gray';
                                $messages = array("", "", "", "");
                                $locations = '';
                                if($result->awb->status1 != null){
                                    $color[0] = 'red';
                                    $messages[0] = 'The item was dispatched by the office at: ' . $result->awb->status1;
                                }
                                if($result->awb->status2 != null){
                                    $color[1] = 'red';
                                    $messages[1] = 'The item is in transit since: ' . $result->awb->status2;
                                    $locations = $result->awb->locations;
                                }
                                if($result->awb->status3 != null){
                                    $color[2] = 'red';
                                    $messages[2] = 'The item is out for delivery since: ' . $result->awb->status3;
                                }
                                if($result->awb->status4 != null){
                                    $color[3] = 'red';
                                    $messages[3] = 'The item was delivered at: ' . $result->awb->status4;
                                }

                                ?>
                                Status:<br>
                                <div style="padding: 10px 14px; border-radius: 20px; background-color: {{ $color[0] }}; text-align: center; display: inline; margin: 30px;"><span style="font-family: 'Nunito', sans-serif">1</span></div><div style="display: inline-block">{{ $messages[0] }}</div><br><br>
                                <div style="padding: 10px 14px; border-radius: 20px; background-color: {{ $color[1] }}; text-align: center; display: inline; margin: 30px;"><span style="font-family: 'Nunito', sans-serif">2</span></div><div style="display: inline-block">{{ $messages[1] }}</div> <div style="display: inline-block">{!! $locations !!}</div><br><br>
                                <div style="padding: 10px 14px; border-radius: 20px; background-color: {{ $color[2] }}; text-align: center; display: inline; margin: 30px;"><span style="font-family: 'Nunito', sans-serif">3</span></div><div style="display: inline-block">{{ $messages[2] }}</div><br><br>
                                <div style="padding: 10px 14px; border-radius: 20px; background-color: {{ $color[3] }}; text-align: center; display: inline; margin: 30px;"><span style="font-family: 'Nunito', sans-serif">4</span></div><div style="display: inline-block">{{ $messages[3] }} <?php if($result->awb->status4 != null) echo $result->office->name; ?></div><br><br>
                                <a href="/item/{{ $result->id }}" class="btn btn-primary">Access</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @elseif($searched)
            <span>No results found</span>
        @endif
    </div>
@endsection
