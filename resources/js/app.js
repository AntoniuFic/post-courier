require('./bootstrap');
window.Vue = require('vue');
const axios = require('axios');
Vue.component('example-component', require('./components/ExampleComponent.vue').default);

const app = new Vue({
    el: '#app',
});


function calculate(){
    var priority = document.getElementById('priority').checked;
    var volume = document.getElementById('volume').value;
    var weight = document.getElementById('weight').value;
    var arrival = document.getElementById('arrival_country').value;
    var departure = document.getElementById('departure_country').value;
    var international = document.getElementById('international1').value;
    var internal = document.getElementById('international0').value;
    if(volume == '')
        volume = 0;
    if(weight == '')
        weight = 0;
    var ret = false;
    if(isNaN(weight)) {
        document.getElementById('weightMessage').innerHTML = 'Please input only numbers!';
        ret = true;
    } else if(weight < 0) {
        document.getElementById('weightMessage').innerHTML = 'Please input only postive values!';
        ret = true;
    }else document.getElementById('weightMessage').innerHTML = '';
    if(isNaN(volume)){
        document.getElementById('volumeMessage').innerHTML = 'Please input only numbers!';
        ret = true;
    }else if(volume < 0) {
        document.getElementById('volumeMessage').innerHTML = 'Please input only postive values!';
        ret = true;
    }else document.getElementById('volumeMessage').innerHTML = '';
    if(ret)
        return;
    var obj = {
        priority: priority,
        volume: volume,
        weight: weight,
        arrival: arrival,
        departure: departure,
        international: international,
        internal: internal
    };
    axios.post('/calculator', obj).then(function (response){
        document.getElementById('price').hidden = false;
        document.getElementById('price').innerHTML = 'Price: ' + response.data + ' EUR';
    }).catch((function (error) {
        console.log(error);
    }));
}

function update(){
    var departure = document.getElementById("departure_continent").value;
    var arrival = document.getElementById("arrival_continent").value;
    var correctInformation = -1;
    for(var i = 0; i < window.fares.length; i++){
        if(window.fares[i].departure_continent == departure && window.fares[i].arrival_continent == arrival){
            correctInformation = i;
            break;
        }
    }
    document.getElementById("external_letter_price_economy").value = window.fares[correctInformation].external_letter_price_economy;
    document.getElementById("external_parcel_price_kg_economy").value = window.fares[correctInformation].external_parcel_price_kg_economy;
    document.getElementById("external_parcel_price_cmcub_economy").value = window.fares[correctInformation].external_parcel_price_cmcub_economy;
    document.getElementById("external_letter_price_priority").value = window.fares[correctInformation].external_letter_price_priority;
    document.getElementById("external_parcel_price_kg_priority").value = window.fares[correctInformation].external_parcel_price_kg_priority;
    document.getElementById("external_parcel_price_cmcub_priority").value = window.fares[correctInformation].external_parcel_price_cmcub_priority;
}

function swtc(type){
    if(type == 0)
        document.getElementById('arrival_country_div').hidden = true;
    else document.getElementById('arrival_country_div').hidden = false;
}

function addButtons(remove, id, newData){
    if(remove){
        for(var i = 0; i < window.results.length; i++){
            document.getElementById('favourites' + window.results[i].id).parameterId = window.results[i].id;
            document.getElementById('favourites' + window.results[i].id).parameterIndex = i;
            if(window.favs[i] == null){
                document.getElementById('favourites' + window.results[i].id).removeEventListener('click', addToFavourites, false);
            }else{
                document.getElementById('favourites' + window.results[i].id).removeEventListener('click', removeFromFavourites, false);
            }
        }
    }
    if(id >= 0 && newData != null)
        window.favs[id] = {...newData};
    else if(id >= 0) window.favs[id] = null;
    for(var i = 0; i < window.results.length; i++){
        if(window.favs[i] == null){
            document.getElementById('favourites' + window.results[i].id).innerHTML = 'Add to favourites';
            document.getElementById('favourites' + window.results[i].id).className = 'btn btn-success';
        }else{
            document.getElementById('favourites' + window.results[i].id).innerHTML = 'Remove from favourites';
            document.getElementById('favourites' + window.results[i].id).className = 'btn btn-danger';
        }
    }
    for(var i = 0; i < window.results.length; i++){
        document.getElementById('favourites' + window.results[i].id).parameterId = window.results[i].id;
        document.getElementById('favourites' + window.results[i].id).parameterIndex = i;
        if(window.favs[i] == null){
            document.getElementById('favourites' + window.results[i].id).addEventListener('click', addToFavourites, false);
        }else{
            document.getElementById('favourites' + window.results[i].id).addEventListener('click', removeFromFavourites, false);
        }
    }
}

function addToFavourites(event){
    var id = event.target.parameterId;
    var index = event.target.parameterIndex;
    if(id === -1)
        return;
    event.target.className = 'btn btn-warning';
    event.target.innerHTML = 'Processing';
    event.target.disabled = true;
    axios.post('/add-fav', {id: id}).then(function (response){
        addButtons(true, index, response.data);
        event.target.disabled = false;
    }).catch((function (error) {
        event.target.className = 'btn btn-danger';
        event.target.innerHTML = 'Error processing';
    }));
}

function removeFromFavourites(event){
    var id = event.target.parameterId;
    var index = event.target.parameterIndex;
    if(id === -1)
        return;
    event.target.className = 'btn btn-warning';
    event.target.innerHTML = 'Processing';
    event.target.disabled = true;
    axios.post('/rem-fav', {id: id}).then(function (response){
        addButtons(true, index, null);
        event.target.disabled = false;
    }).catch((function (error) {
        event.target.className = 'btn btn-danger';
        event.target.innerHTML = 'Error processing';
    }));
}

function openTab(evt){
    let type = evt.target.prm;
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" activated", "");
    }
    evt.currentTarget.className += " activated";
    if(type === 0){
        document.getElementById('international1').checked = false;
        document.getElementById('international0').checked = true;
        swtc(0)
    }else if(type === 1){
        document.getElementById('international1').checked = true;
        document.getElementById('international0').checked = false;
        swtc(1);
    }
}

if(window.location.pathname === '/manager/prices/external'){
    update();

    document.getElementById("departure_continent").addEventListener("change", update);

    document.getElementById("arrival_continent").addEventListener("change", update);
}
if(window.location.pathname === '/manager/news/new'){
    CKEDITOR.replace('news-article');
}
if(window.location.pathname === '/contact'){
    CKEDITOR.replace('contents');
}
if(window.location.pathname === '/calculator'){
    document.getElementById('but1').prm = 0;
    document.getElementById('but2').prm = 1;
    document.getElementById('but1').addEventListener("click", openTab);
    document.getElementById('but2').addEventListener("click", openTab);
    document.getElementById('button').addEventListener("click", calculate);
}
if(window.location.pathname === '/search' && window.auth){
    addButtons(false, -1, null);
}
if(window.location.pathname === '/manager'){
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
}
