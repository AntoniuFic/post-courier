<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'MainController@news')->name('news');

Route::post('/contact', 'HomeController@contact')->name('contact')->middleware('verified');

Route::get('/contact', 'MainController@contact')->name('contact')->middleware('verified');

Route::get('/register-employee', 'PrivilegeController@registerEmployeeForm')->name('register-employee')->middleware('level3');

Route::post('/register-employee', 'PrivilegeController@registerEmployee')->name('register-employee')->middleware('level3');

Route::get('/manager', 'PrivilegeController@manager')->name('manager')->middleware('level2');

Route::get('/search', 'MainController@search')->name('search');

Route::post('/search', 'MainController@searched')->name('search');

Route::get('/privilege-search', 'PrivilegeController@search')->name('privilege-search')->middleware('level2');

Route::post('/privilege-search', 'PrivilegeController@searched')->name('privilege-search')->middleware('level2');

Route::get('/item/{id}', 'PrivilegeController@modify')->middleware('level2');

Route::get('/item/{id}/update-location', 'PrivilegeController@updateLocation')->middleware('level2');

Route::get('/item/{id}/in-transit', 'PrivilegeController@intransit')->middleware('level2');

Route::get('/item/{id}/out-for-delivery', 'PrivilegeController@outForDelivery')->middleware('level2');

Route::get('/item/{id}/delivered', 'PrivilegeController@delivered')->middleware('level2');

Route::get('/item/{id}/delete', 'PrivilegeController@confirmDelete')->middleware('level2');

Route::post('/item/{id}/delete', 'PrivilegeController@delete')->middleware('level2');

Route::get('/register-item', 'PrivilegeController@create')->name('register-item')->middleware('level1');

Route::post('/register-item', 'PrivilegeController@registerItem')->name('register-item')->middleware('level1');

Route::get('/item-registered/{id}', 'PrivilegeController@registeredItem')->name('item-registered')->middleware('level1');

Route::get('/manager/employee/delete/{id}', 'PrivilegeController@deleteEmployee')->name('employee-delete')->middleware('level3');

Route::post('/manager/employee/delete/{id}', 'PrivilegeController@confirmDeleteEmployee')->name('employee-delete')->middleware('level3');

Route::get('/manager/employee/edit/{id}', 'PrivilegeController@editForm')->name('employee-edit')->middleware('level3');

Route::post('/manager/employee/edit/{id}', 'PrivilegeController@editFormSubmit')->name('employee-edit')->middleware('level3');

Route::get('/manager/employee/{level}', 'PrivilegeController@employeeList')->name('employee-list')->middleware('level4');

Route::get('/manager/office/{id}', 'PrivilegeController@officePage')->name('office-list')->middleware('level3');

Route::get('/manager/hub/{id}', 'PrivilegeController@hubPage')->name('hub-list')->middleware('level3');

Route::post('/manager', 'PrivilegeController@internationalManagerEditInternalPrice')->name('manager')->middleware('level7');

Route::get('/manager/prices/external/', 'PrivilegeController@editExternalPrices')->name('edit-external-prices')->middleware('level7');

Route::post('/manager/prices/external/', 'PrivilegeController@editExternalPricesSubmit')->name('edit-external-prices')->middleware('level7');

Route::get('/manager/prices/internal/{id}', 'PrivilegeController@editInternalPrices')->name('edit-internal-prices')->middleware('level5');

Route::post('/manager/prices/internal/{id}', 'PrivilegeController@editInternalPricesSubmit')->name('edit-internal-prices')->middleware('level5');

Route::get('/manager/news/new', 'PrivilegeController@newNewsArticle')->name('create-news')->middleware('level4');

Route::post('/manager/news/new', 'PrivilegeController@newNewsArticleSubmit')->name('create-news')->middleware('level4');

Route::get('/calculator', 'MainController@calculator')->name('calculator');

Route::post('/calculator', 'MainController@calculate')->name('calculator');

Route::post('/add-fav', 'MainController@addFavourites');

Route::post('/rem-fav', 'MainController@removeFavourites');

Route::get('/remove-fav/{id}', 'MainController@removeFavouritesList')->middleware('verified');

Route::get('/favourites', 'MainController@favourites')->name('favourites')->middleware('verified');

Route::get('/feedback-view', 'PrivilegeController@contact')->name('feedback-manager')->middleware('level5');
