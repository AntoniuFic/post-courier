<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('departure_continent');
            $table->unsignedBigInteger('arrival_continent');
            $table->double('external_letter_price_economy');
            $table->double('external_parcel_price_kg_economy');
            $table->double('external_parcel_price_cmcub_economy');
            $table->double('external_letter_price_priority');
            $table->double('external_parcel_price_kg_priority');
            $table->double('external_parcel_price_cmcub_priority');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fares');
    }
}
