<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAwbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('awbs', function (Blueprint $table) {
            $table->unsignedBigInteger('current_location_id');
            $table->integer('current_location_type');
            $table->unsignedBigInteger('next_shipment_id');
            $table->foreign('next_shipment_id')->references('id')->on('shipments');
            $table->text('previous_shipment_ids');
            $table->text('next_transport_ids');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
