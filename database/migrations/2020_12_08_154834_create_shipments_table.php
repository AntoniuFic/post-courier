<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('start');
            $table->unsignedBigInteger('transport_id');
            $table->timestamp('reception')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->boolean('arrived')->default(false);
            $table->double('theoretical_transported_weight');
            $table->unsignedBigInteger('envelope_number');
            $table->unsignedInteger('parcel_number');
            $table->foreign('transport_id')->references('id')->on('transports');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}
