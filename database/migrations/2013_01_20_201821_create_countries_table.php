<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',100);
            $table->unsignedBigInteger('continent_id');
            $table->foreign('continent_id')->references('id')->on('continents');
            $table->double('internal_letter_price_economy');
            $table->double('internal_parcel_price_kg_economy');
            $table->double('internal_parcel_price_cmcub_economy');
            $table->double('internal_letter_price_priority');
            $table->double('internal_parcel_price_kg_priority');
            $table->double('internal_parcel_price_cmcub_priority');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
