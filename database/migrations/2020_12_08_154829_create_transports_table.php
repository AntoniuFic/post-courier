<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->unsignedBigInteger('start_hub_id');
            $table->unsignedBigInteger('end_hub_id');
            $table->string('periodicity');
            $table->double('cost_per_envelope');
            $table->double('cost_per_kg');
            $table->unsignedInteger('est_duration');
            $table->integer('type');
            $table->double('max_weight');
            $table->double('max_height');
            $table->double('max_width');
            $table->double('max_depth');
            $table->unsignedInteger('max_envelopes');
            $table->boolean('due_delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transports');
    }
}
