<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('surname', 100);
            $table->string('street', 100);
            $table->string('number', 100);
            $table->string('block', 100)->nullable();
            $table->string('staircase', 100)->nullable();
            $table->string('apartment', 100)->nullable();
            $table->string('city', 100);
            $table->string('county', 100);
            $table->double('price');
            $table->double('weight')->nullable();
            $table->double('volume')->nullable();
            $table->boolean('priority');
            $table->unsignedBigInteger('country_id');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->unsignedBigInteger('office_id');
            $table->unsignedBigInteger('zipcode_id');
            $table->unsignedBigInteger('awb_id');
            $table->foreign('office_id')->references('id')->on('offices');
            $table->foreign('zipcode_id')->references('id')->on('zip_codes');
            $table->foreign('awb_id')->references('id')->on('awbs');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
