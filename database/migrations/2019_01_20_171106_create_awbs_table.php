<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAwbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('awbs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('number', 100)->unique();
            $table->string('locations', 255);
            $table->timestamp('status1')->nullable()->default(null);
            $table->timestamp('status2')->nullable()->default(null);
            $table->timestamp('status3')->nullable()->default(null);
            $table->timestamp('status4')->nullable()->default(null);
            $table->integer('next_location_type');
            $table->unsignedBigInteger('next_location_id');
            $table->unsignedBigInteger('office_id');
            $table->foreign('office_id')->references('id')->on('offices');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('awbs');
    }
}
